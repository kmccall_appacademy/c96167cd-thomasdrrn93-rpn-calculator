class RPNCalculator
    attr_accessor :stack

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    raise "calculator is empty" if @stack.count < 2
    right = @stack.pop
    left = @stack.pop
    @stack << left + right
  end

  def minus
    raise "calculator is empty" if @stack.count < 2
    right = @stack.pop
    left = @stack.pop
    @stack << left - right
  end

  def times
    raise "calculator is empty" if @stack.count < 2
    right = @stack.pop
    left = @stack.pop
    @stack << left * right
  end

  def divide
    raise "calculator is empty" if @stack.count < 2
    right = @stack.pop
    left = @stack.pop
    @stack << (left * 1.0) / right
  end

  def value
    @stack.last
  end

  def tokens(string)
    numbers = "0123456789"
    string.delete(" ").chars.map do |el|
      if numbers.include?(el)
        el.to_i
      else
        el.to_sym
      end
    end
  end

  def evaluate(string)
    syms = [:*, :/, :+, :-]
    self.tokens(string).each do |el|
      if syms.include?(el)
        if el == :*
          self.times
        elsif el == :/
          self.divide
        elsif el == :+
          self.plus
        else
          self.minus
        end
      else
        self.push(el)
      end
    end
    self.value
  end
end
